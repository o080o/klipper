// Polar kinematics stepper pulse time generation
//
// Copyright (C) 2018-2019  Kevin O'Connor <kevin@koconnor.net>
//
// This file may be distributed under the terms of the GNU GPLv3 license.

#include <math.h> // sqrt
#include <stddef.h> // offsetof
#include <stdlib.h> // malloc
#include <string.h> // memset
#include "compiler.h" // __visible
#include "itersolve.h" // struct stepper_kinematics
#include "trapq.h" // move_get_coord
#include "pyhelper.h" // errorf

#define EPSILON 0.0001 // for comparing floats around the kinematic singularity (distance==0)

struct scara_stepper {
    struct stepper_kinematics sk;
    double proximal_radius, distal_radius;
    double center_x, center_y;
};

static double
scara_cosinelaw(double a, double b, double c)
{
    return acos((a*a + b*b - c*c) / (2*a*b));
}

static double
scara_stepper_proximal_angle_calc_position(struct stepper_kinematics *sk, struct move *m
                                   , double move_time)
{

    struct scara_stepper *ss = container_of(sk, struct scara_stepper, sk);
    struct coord c = move_get_coord(m, move_time);
    // translate so the center is 0,0
    double x = c.x - ss->center_x;
    double y = c.y - ss->center_y;
    double distance = sqrt(x*x+y*y);
    // there are better ways to do this, but for now, let's see if this is causing our NaN
    if(distance < EPSILON) {
        errorf("kin_scara distance=0, singularity!");
        return 0.0;
    } else {
        double d1 = atan2(y,x); // angle between the x axis and the triangle defined by the center, the end effector, and the elbow.
        double d2 = scara_cosinelaw(distance, ss->proximal_radius, ss->distal_radius); // angle between the proximal arm and the line between the center and the end effector
        double a1 = d1 + d2;
	//errorf("kin_scara promimal=%f, d1=%f, d2=%f pos=(%f,%f)", a1, d1, d2, y,x);
        return a1;
    }
}


static double
scara_stepper_distal_angle_calc_position(struct stepper_kinematics *sk, struct move *m
                                  , double move_time)
{

    struct scara_stepper *ss = container_of(sk, struct scara_stepper, sk);
    struct coord c = move_get_coord(m, move_time);
    // translate so the center is 0,0
    double x = c.x - ss->center_x;
    double y = c.y - ss->center_y;
    double distance = sqrt(x*x+y*y);
    // there are better ways to do this, but for now, let's see if this is causing our NaN
    if(distance < EPSILON) {
        errorf("kin_scara distance=0, singularity!");
        return 0.0;
    } else {
        double a2 = scara_cosinelaw(ss->proximal_radius, ss->distal_radius, distance);
        //errorf("kin_scara distal=%f pos=(%f,%f)", a2, x, y);
        return a2;
    }
}

static void
scara_stepper_angle_post_fixup(struct stepper_kinematics *sk)
{
    // Normalize the stepper angle
    if (sk->commanded_pos < -M_PI)
        sk->commanded_pos += 2 * M_PI;
    else if (sk->commanded_pos > M_PI)
        sk->commanded_pos -= 2 * M_PI;
}

struct stepper_kinematics * __visible
scara_stepper_alloc(char type, double proximal_radius, double distal_radius, double center_x, double center_y)
{
    struct scara_stepper *ss = malloc(sizeof(*ss));
    memset(ss, 0, sizeof(*ss));
    ss->proximal_radius = proximal_radius;
    ss->distal_radius = distal_radius;
    ss->center_x = center_x;
    ss->center_y = center_y;

    if (type == 'p') {
        ss->sk.calc_position_cb = scara_stepper_proximal_angle_calc_position;
        ss->sk.post_cb = scara_stepper_angle_post_fixup;
    } else if (type == 'd') {
        ss->sk.calc_position_cb = scara_stepper_distal_angle_calc_position;
        ss->sk.post_cb = scara_stepper_angle_post_fixup;
    }
    ss->sk.active_flags = AF_X | AF_Y;
    return &ss->sk;
}
