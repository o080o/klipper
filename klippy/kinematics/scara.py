# Code for handling the kinematics of polar robots
#
# Copyright (C) 2018-2021  Kevin O'Connor <kevin@koconnor.net>
#
# This file may be distributed under the terms of the GNU GPLv3 license.
import logging, math
import stepper
import chelper


class ScaraKinematics:
    def __init__(self, toolhead, config):
        # Setup axis steppers
        rail_proximal = stepper.LookupMultiRail(config.getsection('stepper_proximal'),
                                             units_in_radians=True)
        rail_distal = stepper.LookupMultiRail(config.getsection('stepper_distal'),
                                             units_in_radians=True)
        rail_z = stepper.LookupMultiRail(config.getsection('stepper_z'))

        scara_config = config.getsection('scara_config')

        self.proximal_radius = config.get
        # TODO configure this somewhere..
        self.proximal_radius = 64.2
        self.distal_radius = 75.0
        self.homing_xy = False

        rail_proximal.setup_itersolve('scara_stepper_alloc', b'p', self.proximal_radius, self.distal_radius, 0.0, 0.0)
        rail_distal.setup_itersolve('scara_stepper_alloc', b'd', self.proximal_radius, self.distal_radius, 0.0, 0.0)
        rail_z.setup_itersolve('cartesian_stepper_alloc', b'z')
        self.rails = [rail_proximal, rail_distal, rail_z]
        self.steppers = [] + rail_proximal.get_steppers() + rail_distal.get_steppers() + rail_z.get_steppers()
        for s in self.get_steppers():
            s.set_trapq(toolhead.get_trapq())
            toolhead.register_step_generator(s.generate_steps)
        config.get_printer().register_event_handler("stepper_enable:motor_off",
                                                    self._motor_off)
        # Setup boundary checks
        max_velocity, max_accel = toolhead.get_max_velocity()
        self.max_z_velocity = config.getfloat(
            'max_z_velocity', max_velocity, above=0., maxval=max_velocity)
        self.max_z_accel = config.getfloat(
            'max_z_accel', max_accel, above=0., maxval=max_accel)
        self.limit_z = (1.0, -1.0)
        self.limit_xy2 = -1.
        max_xy = self.proximal_radius + self.distal_radius
        min_z, max_z = self.rails[2].get_range()
        self.axes_min = toolhead.Coord(-max_xy, -max_xy, min_z, 0.)
        self.axes_max = toolhead.Coord(max_xy, max_xy, max_z, 0.)
    def get_steppers(self):
        return list(self.steppers)
    def calc_position(self, stepper_positions):
        proximal_angle = stepper_positions[self.rails[0].get_name()]
        distal_angle = stepper_positions[self.rails[1].get_name()]
        z = stepper_positions[self.rails[2].get_name()]
        x, y = self.forward_kinematics(proximal_angle, distal_angle)
        return [x,y,z]
    def forward_kinematics(self, proximal_angle, distal_angle):
        elbow_x = math.cos(proximal_angle) * self.proximal_radius
        elbow_y = math.sin(proximal_angle) * self.proximal_radius
        x = elbow_x + math.cos(distal_angle) * self.distal_radius
        y = elbow_y + math.sin(distal_angle) * self.distal_radius
        logging.info("forward kinematics: pa: %0.5f da:%0.5f ex:%0.5f ey:%0.5f x:%0.5f y:%0.5f", proximal_angle, distal_angle, elbow_x, elbow_y, x,y)
        return [x,y]
    def set_position(self, newpos, homing_axes):
        logging.info("TODO: NYI set_position", newpos)
        for s in self.steppers:
            # TODO seems like I should convert cartesian => stepper angles first?
            s.set_position(newpos)
        if 2 in homing_axes:
            self.limit_z = self.rails[2].get_range()
        if 0 in homing_axes or 1 in homing_axes:
            self.limit_xy2 = pow(self.proximal_radius + self.distal_radius, 2)
    def note_z_not_homed(self):
        # Helper for Safe Z Home
        self.limit_z = (1.0, -1.0)
    def home_axis(self, homing_state, axis, rail):
        # Determine movement
        position_min, position_max = rail.get_range()
        hi = rail.get_homing_info()
        homepos = [None, None, None, None]
        homepos[axis] = hi.position_endstop
        forcepos = list(homepos)
        if hi.positive_dir:
            forcepos[axis] -= 1.5 * (hi.position_endstop - position_min)
        else:
            forcepos[axis] += 1.5 * (position_max - hi.position_endstop)
        # Perform homing
        logging.info("Homing axis %d " + "({0},{1},{2})".format(*forcepos) + " -> ({0},{1},{2})".format(*homepos) + "speed:%f", axis, hi.speed )
        homing_state.home_rails([rail], forcepos, homepos)
    def home_xy3(self, homing_state):
        # Determine movement
        proximal_info = self.rails[0].get_homing_info()
        distal_info = self.rails[1].get_homing_info()
        # get the theoretical homed position
        proximal_angle = proximal_info.position_endstop
        distal_angle = distal_info.position_endstop
        home_pos = self.forward_kinematics(proximal_angle, distal_angle)
        # get the rails+steppers we want to home
        rails = [self.rails[0], self.rails[1]]
        steppers = [self.steppers[0], self.steppers[1]]
        self.homing_xy = True

        # home our "non-linear" rails!
        # first, create some new info for each stepper. mainly, setup a new, linear, kinematics model for it, that we can use later.
        ffi_main, ffi_lib = chelper.get_ffi()
        axes = [b'x', b'y', b'z']
        stepper_info = {}
        for rail_index, rail in enumerate(rails):
            for stepper in rail.get_steppers():
                stepper_info[stepper] = {
                        "kinematics": ffi_main.gc(
                            ffi_lib.cartesian_stepper_alloc(axes[rail_index]),
                            ffi_lib.free
                        ),
                        "original_kinematics": None, # will be populated later
                }

        # swap a different kinematic temporarily
        logging.info("Swapping kinematics...");
        for stepper in steppers:
            info = stepper_info[stepper]
            info["original_kinematics"] = stepper.set_stepper_kinematics(info["kinematics"])
            stepper.set_position((0., 0., 0.))
        # feel free to home, "as if" it were a true cartesian kinematics, where homing one axis only affects one stepper at a time!
        logging.info("Homing as linear axes...");
        for rail_index, rail in enumerate(rails):
            rail_stepper = rail.get_steppers()[0]
            logging.info("Homing axis %d using pseudostepper %s", rail_index, rail_stepper._name);
            self.home_axis(homing_state, rail_index, rail)
        # swap to original kinematics
        logging.info("Swapping kinematics back...");
        for stepper in steppers:
            stepper.set_stepper_kinematics(stepper_info[stepper]["original_kinematics"])
        self.homing_xy = False
        logging.info("Done.");
        current_position = homing_state.toolhead.get_position()
        new_position = home_pos + current_position[2:]
        homing_state.toolhead.set_position(new_position, homing_axes=[0,1])
        #homing_state.toolhead.flush_step_generation()

    def home_xy2(self, homing_state):
        toolhead = homing_state.toolhead
        printer = homing_state.printer

        # Determine movement
        proximal_min, proximal_max = self.rails[0].get_range()
        distal_min, distal_max = self.rails[0].get_range()
        proximal_info = self.rails[0].get_homing_info()
        distal_info = self.rails[1].get_homing_info()

        # Alter kinematics class to think printer is at forcepos
        #homing_axes = [axis for axis in range(3) if forcepos[axis] is not None]

        # get the theoretical homed position
        proximal_angle = proximal_info.position_endstop
        distal_angle = distal_info.position_endstop
        homepos = self.forward_kinematics(proximal_angle, distal_angle)
        
        # determine a kinematic position that would force the arm to reach the homing position in the desired direction.
        forcepos = [None, None]
        # TODO will this only move one axis at a time?? or, will it try to make a straight line, and move the distal/proximal angle to compensate?
        #if rail == self.rails[0]:
            #logging.info("homing proximal rail...")
            #forcepos = self.forward_kinematics(proximal_max, distal_angle)
        #if rail == self.rails[1]:
            #logging.info("homing distal rail...")
            #forcepos = self.forward_kinematics(proximal_angle, distal_max)

        startpos_proximal = self.forward_kinematics(proximal_max, distal_angle)
        startpos_distal = self.forward_kinematics(proximal_angle, distal_max)

        # Perform homing
        logging.info("Homing Angles:(%0.5f,%0.5f) Force Angles: (%0.5f,%0.5f)", proximal_angle, distal_angle, proximal_max, distal_max)

        startpos = startpos_proximal + [0, 0]
        homepos = homepos + [0,0]
        # Perform first proximal home
        rail = self.rails[0]
        endstops = [es for rail in [rail] for es in rail.get_endstops()]
        hi = rail.get_homing_info()
        toolhead.set_position(startpos, homing_axes=[0,1])
        hmove = homing_state.homing_move(endstops)
        hmove.homing_move(homepos, hi.speed)
        logging.info("proximal homed. homing distal...");

        startpos = startpos_distal + [0, 0]
        homepos = homepos + [0,0]
        # Perform first distal home
        rail = self.rails[1]
        endstops = [es for rail in [rail] for es in rail.get_endstops()]
        hi = rail.get_homing_info()
        toolhead.set_position(startpos, homing_axes=[0,1])
        hmove = homing_state.homing_move(endstops)
        hmove.homing_move(homepos, hi.speed)
        logging.info("homing moves over! retracting...");


        rail = self.rails[0]
        hi = rail.get_homing_info()
        # Perform second home
        if hi.retract_dist:
            logging.info("retracting...");
            # Retract
            retractpos = self.forward_kinematics(proximal_angle + hi.retract_dist, distal_angle) + [0, 0]
            toolhead.move(retractpos, hi.retract_speed)
            logging.info("retracted!");
            logging.info("fine homing...");
            # Home again
            startpos = startpos_proximal + [0, 0]
            homepos = homepos + [0,0]
            toolhead.set_position(startpos, homing_axes=[0,1])
            hmove = homing_state.homing_move(endstops)
            hmove.homing_move(homepos, hi.second_homing_speed)
            if hmove.check_no_movement() is not None:
                raise printer.command_error(
                    "Endstop %s still triggered after retract"
                    % (hmove.check_no_movement(),))
        # Signal home operation complete
        logging.info("flushing...");
        toolhead.flush_step_generation()
        #self.trigger_mcu_pos = {sp.stepper_name: sp.trig_pos
        #                        for sp in hmove.stepper_positions}
        #self.adjust_pos = {}
        #self.printer.send_event("homing:home_rails_end", self, rails)
    def home_xy(self, homing_state, rail):
        # Determine movement
        proximal_min, proximal_max = self.rails[0].get_range()
        distal_min, distal_max = self.rails[0].get_range()
        proximal_info = self.rails[0].get_homing_info()
        distal_info = self.rails[1].get_homing_info()

        # get the theoretical homed position
        proximal_angle = proximal_info.position_endstop
        distal_angle = distal_info.position_endstop
        homepos = self.forward_kinematics(proximal_angle, distal_angle)

        
        # determine a kinematic position that would force the arm to reach the homing position in the desired direction.
        forcepos = [None, None]
        # TODO will this only move one axis at a time?? or, will it try to make a straight line, and move the distal/proximal angle to compensate?
        if rail == self.rails[0]:
            logging.info("homing proximal rail...")
            forcepos = self.forward_kinematics(proximal_max, distal_angle)
        if rail == self.rails[1]:
            logging.info("homing distal rail...")
            forcepos = self.forward_kinematics(proximal_angle, distal_max)

        # Perform homing
        logging.info("Homing Angles:(%0.5f,%0.5f) Force Angles: (%0.5f,%0.5f)", proximal_angle, distal_angle, proximal_max, distal_max)
        homing_state.home_rails([rail], forcepos + [None, None], homepos + [None, None])
    def home(self, homing_state):
        # Always home XY together
        homing_axes = homing_state.get_axes()
        home_xy = 0 in homing_axes or 1 in homing_axes
        home_z = 2 in homing_axes
        logging.info("Homing...");
        logging.info("Homing... %d, %d", home_xy, home_z)
        updated_axes = []
        if home_xy:
            updated_axes = [0, 1]
        if home_z:
            updated_axes.append(2)
        homing_state.set_axes(updated_axes)
        # Do actual homing
        if home_xy:
            # for now, just always assume the printer is already homed to 0,0 degrees
            #toolhead = homing_state.printer.lookup_object('toolhead')
            #toolhead.get_last_move_time()
            #curpos = toolhead.get_position()
            #x = abs(self.proximal_radius - self.distal_radius)
            #y = 0
            #z = curpos[2]
            #logging.info("SET_KINEMATIC_POSITION pos=%.3f,%.3f,%.3f", x, y, z)
            #toolhead.set_position([x, y, z, curpos[3]], homing_axes=(0, 1))
            self.home_xy3(homing_state)
            # restore limits now that we are homed.
            self.limit_xy2 = pow(self.proximal_radius + self.distal_radius, 2)
        if home_z:
            self.home_axis(homing_state, 2, self.rails[2]) #TODO uncomment this line for z-homing again..
            # restore limits now that we are homed.
            self.limit_z = self.rails[0].get_range()
    def _motor_off(self, print_time):
        self.limit_z = (1.0, -1.0)
        self.limit_xy2 = -1.
    def check_move(self, move):
        # TODO we could also check if the move passes through the center..
        end_pos = move.end_pos
        xy2 = end_pos[0]**2 + end_pos[1]**2
        epsilon = 0.001
        logging.info("Checking move... (%f,%f,%f), [%0.4f, %0.4f]", end_pos[0], end_pos[1], end_pos[2], xy2, self.limit_xy2);
        moving_xy = move.axes_d[0] or move.axes_d[1]
        # we modify the kinematics while homing xy, so we shouldn't use this kinematic class to judge it.
        if moving_xy and not self.homing_xy:
            if xy2 > self.limit_xy2 + epsilon:
                if self.limit_xy2 < 0.:
                    raise move.move_error("Must home axis first")
                raise move.move_error("Move outside radial limit")
        if move.axes_d[2]:
            if end_pos[2] < self.limit_z[0] or end_pos[2] > self.limit_z[1]:
                if self.limit_z[0] > self.limit_z[1]:
                    raise move.move_error("Must home axis first")
                raise move.move_error()
            # Move with Z - update velocity and accel for slower Z axis
            z_ratio = move.move_d / abs(move.axes_d[2])
            move.limit_speed(self.max_z_velocity * z_ratio,
                             self.max_z_accel * z_ratio)
    def get_status(self, eventtime):
        xy_home = "xy" if self.limit_xy2 >= 0. else ""
        z_home = "z" if self.limit_z[0] <= self.limit_z[1] else ""
        return {
            'homed_axes': xy_home + z_home,
            'axis_minimum': self.axes_min,
            'axis_maximum': self.axes_max,
        }

def load_kinematics(toolhead, config):
    return ScaraKinematics(toolhead, config)
